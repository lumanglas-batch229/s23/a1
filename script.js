// console.log('hello');

let trainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends : {
		hoenn : ["May", "Max"],
		kanto : ["Brock", "Misty"]
	},
	talk : function () {
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer);
console.log("Result of dot notation");
console.log(trainer.name);
console.log(trainer.age);

console.log("Result of square bracket notation:");
console.log(trainer.pokemon);
console.log(trainer.friends);

console.log("Result of talk method:");
trainer.talk()


function pokemonType(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

this.tackle = function	(targetPokemon) {
	console.log(this.name + " tackled " + targetPokemon.name )
}
this.faint = function () {
	console.log(this.name + " fainted!")
	}
}

let newPokemon1 = new pokemonType ("Pikachu", 12)
console.log(newPokemon1);

let newPokemon2 = new pokemonType ("Geodude", 8)
console.log(newPokemon2);

let newPokemon3 = new pokemonType ("Mewtwo", 100)
console.log(newPokemon3);

newPokemon2.tackle(newPokemon1)
newPokemon3.tackle(newPokemon2)
newPokemon3.faint()


